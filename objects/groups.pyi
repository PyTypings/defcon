from _typeshed import Incomplete
from defcon.objects.base import BaseDictObject as BaseDictObject
from defcon.tools.representations import glyphToKerningSide1GroupsRepresentationFactory as glyphToKerningSide1GroupsRepresentationFactory, glyphToKerningSide2GroupsRepresentationFactory as glyphToKerningSide2GroupsRepresentationFactory, kerningSide1GroupsRepresentationFactory as kerningSide1GroupsRepresentationFactory, kerningSide2GroupsRepresentationFactory as kerningSide2GroupsRepresentationFactory

class Groups(BaseDictObject):
    changeNotificationName: str
    beginUndoNotificationName: str
    endUndoNotificationName: str
    beginRedoNotificationName: str
    endRedoNotificationName: str
    setItemNotificationName: str
    deleteItemNotificationName: str
    clearNotificationName: str
    updateNotificationName: str
    representationFactories: Incomplete
    def __init__(self, font: Incomplete | None = ...) -> None: ...
    def getParent(self): ...
    font: Incomplete
    def endSelfNotificationObservation(self) -> None: ...
