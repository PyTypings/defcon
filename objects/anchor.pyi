from _typeshed import Incomplete
from defcon.objects.base import BaseDictCompareObject as BaseDictCompareObject
from defcon.objects.color import Color as Color
from defcon.tools.identifiers import makeRandomIdentifier as makeRandomIdentifier

class Anchor(BaseDictCompareObject):
    changeNotificationName: str
    representationFactories: Incomplete
    glyph: Incomplete
    x: Incomplete
    y: Incomplete
    name: Incomplete
    color: Incomplete
    identifier: Incomplete
    def __init__(self, glyph: Incomplete | None = ..., anchorDict: Incomplete | None = ...) -> None: ...
    def getParent(self): ...
    font: Incomplete
    layerSet: Incomplete
    layer: Incomplete
    identifiers: Incomplete
    def generateIdentifier(self): ...
    def move(self, values) -> None: ...
    def endSelfNotificationObservation(self) -> None: ...
