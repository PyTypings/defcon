from _typeshed import Incomplete
from defcon.objects.base import BaseObject as BaseObject

class Features(BaseObject):
    changeNotificationName: str
    beginUndoNotificationName: str
    endUndoNotificationName: str
    beginRedoNotificationName: str
    endRedoNotificationName: str
    representationFactories: Incomplete
    def __init__(self, font: Incomplete | None = ...) -> None: ...
    def getParent(self): ...
    font: Incomplete
    text: Incomplete
    def endSelfNotificationObservation(self) -> None: ...
    def getDataForSerialization(self, **kwargs): ...
    def setDataFromSerialization(self, data) -> None: ...
