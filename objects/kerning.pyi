from _typeshed import Incomplete
from defcon.objects.base import BaseDictObject as BaseDictObject

class Kerning(BaseDictObject):
    changeNotificationName: str
    beginUndoNotificationName: str
    endUndoNotificationName: str
    beginRedoNotificationName: str
    endRedoNotificationName: str
    setItemNotificationName: str
    deleteItemNotificationName: str
    clearNotificationName: str
    updateNotificationName: str
    representationFactories: Incomplete
    def __init__(self, font: Incomplete | None = ...) -> None: ...
    def getParent(self): ...
    font: Incomplete
    def get(self, pair, default: int = ...): ...
    def find(self, pair, default: int = ...): ...
    def endSelfNotificationObservation(self) -> None: ...
