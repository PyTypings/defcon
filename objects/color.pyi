from _typeshed import Incomplete

class Color(str):
    def __new__(self, value): ...
    def __iter__(self): ...
    r: Incomplete
    g: Incomplete
    b: Incomplete
    a: Incomplete
