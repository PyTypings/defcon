from _typeshed import Incomplete
from defcon.objects.base import BaseDictCompareObject as BaseDictCompareObject
from defcon.objects.color import Color as Color
from defcon.tools.identifiers import makeRandomIdentifier as makeRandomIdentifier

class Guideline(BaseDictCompareObject):
    changeNotificationName: str
    representationFactories: Incomplete
    font: Incomplete
    glyph: Incomplete
    x: Incomplete
    y: Incomplete
    angle: Incomplete
    name: Incomplete
    color: Incomplete
    identifier: Incomplete
    def __init__(self, font: Incomplete | None = ..., glyph: Incomplete | None = ..., guidelineDict: Incomplete | None = ...) -> None: ...
    def getParent(self): ...
    layerSet: Incomplete
    layer: Incomplete
    identifiers: Incomplete
    def generateIdentifier(self): ...
    def endSelfNotificationObservation(self) -> None: ...
