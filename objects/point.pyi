from _typeshed import Incomplete

class Point:
    def __init__(self, coordinates, segmentType: Incomplete | None = ..., smooth: bool = ..., name: Incomplete | None = ..., identifier: Incomplete | None = ...) -> None: ...
    segmentType: Incomplete
    x: Incomplete
    y: Incomplete
    smooth: Incomplete
    name: Incomplete
    def move(self, values) -> None: ...
    identifier: Incomplete
