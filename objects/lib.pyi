from _typeshed import Incomplete
from defcon.objects.base import BaseDictObject as BaseDictObject

class Lib(BaseDictObject):
    changeNotificationName: str
    beginUndoNotificationName: str
    endUndoNotificationName: str
    beginRedoNotificationName: str
    endRedoNotificationName: str
    setItemNotificationName: str
    deleteItemNotificationName: str
    clearNotificationName: str
    updateNotificationName: str
    representationFactories: Incomplete
    font: Incomplete
    layer: Incomplete
    glyph: Incomplete
    def __init__(self, font: Incomplete | None = ..., layer: Incomplete | None = ..., glyph: Incomplete | None = ...) -> None: ...
    def getParent(self): ...
    layerSet: Incomplete
    def endSelfNotificationObservation(self) -> None: ...
