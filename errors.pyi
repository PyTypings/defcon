from _typeshed import Incomplete

class DefconError(Exception):
    report: Incomplete
