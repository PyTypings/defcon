from _typeshed import Incomplete
from defcon.errors import DefconError as DefconError
from defcon.objects.anchor import Anchor as Anchor
from defcon.objects.color import Color as Color
from defcon.objects.component import Component as Component
from defcon.objects.contour import Contour as Contour
from defcon.objects.features import Features as Features
from defcon.objects.font import Font as Font
from defcon.objects.glyph import Glyph as Glyph, addRepresentationFactory as addRepresentationFactory, removeRepresentationFactory as removeRepresentationFactory
from defcon.objects.groups import Groups as Groups
from defcon.objects.guideline import Guideline as Guideline
from defcon.objects.image import Image as Image
from defcon.objects.info import Info as Info
from defcon.objects.kerning import Kerning as Kerning
from defcon.objects.layer import Layer as Layer
from defcon.objects.layerSet import LayerSet as LayerSet
from defcon.objects.layoutEngine import LayoutEngine as LayoutEngine
from defcon.objects.lib import Lib as Lib
from defcon.objects.point import Point as Point
from defcon.objects.uniData import UnicodeData as UnicodeData

def registerRepresentationFactory(cls, name, factory, destructiveNotifications: Incomplete | None = ...) -> None: ...
def unregisterRepresentationFactory(cls, name) -> None: ...
