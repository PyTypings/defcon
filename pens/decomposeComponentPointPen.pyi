from _typeshed import Incomplete
from defcon.pens.glyphObjectPointPen import GlyphObjectPointPen as GlyphObjectPointPen
from defcon.pens.transformPointPen import TransformPointPen as TransformPointPen

class DecomposeComponentPointPen(GlyphObjectPointPen):
    def __init__(self, glyph, layer) -> None: ...
    def addComponent(self, baseGlyphName, transformation, identifier: Incomplete | None = ..., **kwargs) -> None: ...
