from _typeshed import Incomplete

characters: Incomplete
identifierLength: int
identifierRange: Incomplete

def makeRandomIdentifier(existing, recursionDepth: int = ...): ...
