from _typeshed import Incomplete

openValue: Incomplete
line: Incomplete
value: Incomplete
name: Incomplete
category: Incomplete
closeValue = value
openValue = value
orderedScripts: Incomplete
orderedBlocks: Incomplete
orderedCategories: Incomplete

def decompositionBase(value): ...
def openRelative(value): ...
def closeRelative(value): ...
def script(value): ...
def block(value): ...
