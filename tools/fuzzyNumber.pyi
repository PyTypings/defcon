from _typeshed import Incomplete

class FuzzyNumber:
    value: Incomplete
    threshold: Incomplete
    def __init__(self, value, threshold) -> None: ...
    def __lt__(self, other): ...
    def __eq__(self, other): ...
    def __hash__(self): ...
