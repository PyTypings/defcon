from fontTools.misc.arrayTools import unionRect as unionRect

def kerningSide1GroupsRepresentationFactory(groups): ...
def kerningSide2GroupsRepresentationFactory(groups): ...
def glyphToKerningSide1GroupsRepresentationFactory(groups): ...
def glyphToKerningSide2GroupsRepresentationFactory(groups): ...
def glyphAreaRepresentationFactory(glyph): ...
def contourBoundsRepresentationFactory(obj): ...
def contourControlPointBoundsRepresentationFactory(obj): ...
def contourAreaRepresentationFactory(contour): ...
def contourFlattenedRepresentationFactory(contour, approximateSegmentLength: int = ..., segmentLines: bool = ...): ...
def componentBoundsRepresentationFactory(obj): ...
def componentPointBoundsRepresentationFactory(obj): ...
